# pier-admin-ui
This project implements the UI of the pier amdin services above the pier-admin API.
It is implemented using VueJS.

## Local project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
#### Build on your local arch
```
npm run build
```
## Docker setup
### On your local devt machine
If you want to build an image for your local tests:
```
npm run docker-build
```
then run the image locally with
```
npm run docker-run
```

### multiarch docker build
You can build a multiarch image (linux/amd64, linux/arm/v7 and linux/arm64) with
```
./build.sh [-p]
```
The -p flags pushes the image to the docker hub. Not using it is just for checking that the image can be built but it won't be usable after that.

Using npm:
```
npm run docker-buildx
```
or if you want to push after the build:
```
npm run 'docker-buildx&push'
```
The UI can the be accessed on http://localhost:9090/admin
### Environment
You can set the following variable from `.env.local` for your local tests outside of docker (i.e. managed with `npm run serve`).

* `VUE_APP_API_URL`: the API to the pier-admin API. Should look like `https://<YOUR DOMAIN OR IP>/admin/api`or `http://localhost:4000` if you test also the API locally
* `BASE_URL` : the base url of the UI, no need to change the `/` default value normally
* `PORT`: the port of the UI, change to your will but you must change the `CORS_DOMAINS` variable on the API side accordingly

In the Docker image, you need to change `.env.production`: 
* the `BASE_URL` must be forced to /admin or you will need to change the location in entrypoint.sh. 
* The `PORT` can be changed
* The `VUE_APP_API_URL` is computed dynamically in the docker-compose.yml file
## Reverse proxying with Apache
As an example, here is the ProxyPass config I'm using for reverse proxying the Pier Admin UI and API from a vhost with its own domain.
First

` define PierInternalURI http://<the IP of your RPi on the LAN>`

Then, assuming you want to access the UI on /admin and you kept the standard parameters for the API, add the following lines in the VHost definition:
```
  ProxyPass "/" "${PierInternalURI}/"
  ProxyPassReverse "/" "${PierInternalURI}/"
```

# Local testing
If you want to test pier-admin + pier-admin-ui locally (no docker, semi fake test envt), please follow the instructions on the [pier-admin wiki](https://gitlab.com/pierhost/pier-admin/-/wikis/home)
