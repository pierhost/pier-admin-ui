import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import { store, key } from "./store";
import PrimeVue from "primevue/config";
//import "primevue/resources/themes/saga-blue/theme.css";
import "@/css/pier-theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.min.css";
import ToastService from "primevue/toastservice";

createApp(App)
  .use(PrimeVue)
  .use(store, key)
  .use(router)
  .use(ToastService)
  .mount("#app");
