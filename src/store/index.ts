import { InjectionKey } from "vue";
import { createStore, useStore as baseUseStore, Store } from "vuex";
import createPersistedState from "vuex-persistedstate";
import auth from "./modules/auth";

export type InterceptorPromise = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  resolve: (value: any) => void;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  reject: (reason?: any) => void;
};
export interface AuthState {
  networkError: boolean;
  firstLogin: boolean;
  logged: boolean;
  changingPassword: boolean;
  failedQueue: InterceptorPromise[];
}
interface RootState {
  auth: AuthState;
}
// define injection key
export const key: InjectionKey<Store<RootState>> = Symbol();

export const store = createStore<RootState>({
  modules: auth,
  plugins: [createPersistedState()], // really useful ?
});

// define your own `useStore` composition function
export function useStore(): Store<RootState> {
  return baseUseStore(key);
}
