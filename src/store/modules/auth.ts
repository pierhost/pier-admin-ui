/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { http } from "@/utils/http-commons";
import { Module, ModuleTree } from "vuex";
import { AuthState, InterceptorPromise } from "@/store";

const authModule: Module<AuthState, unknown> = {
  mutations: {
    logIn(state: AuthState): void {
      state.logged = true;
      console.debug("logged in");
      // normally, here the cookie should be ok, we can resend failed requests
      if (state.failedQueue) {
        console.debug("Processing failed queue...");
        state.failedQueue.forEach((prom) => {
          console.debug(
            "Resolving promise from failed queue...",
            JSON.stringify(prom)
          );
          prom.resolve(true);
        });
      }
    },
    logOut(state: AuthState): void {
      state.logged = false;
      console.debug("logged out");
    },
    firstLogIn(state: AuthState, first: boolean): void {
      state.firstLogin = first;
      if (first) state.logged = false;
      console.debug("first login? ", first);
    },
    changingPassword(state: AuthState, changing: boolean): void {
      state.changingPassword = changing;
      console.debug("changing password", changing);
    },
    addFailed(state: AuthState, failed: InterceptorPromise): void {
      if (state.failedQueue == undefined) state.failedQueue = [];
      state.failedQueue.push(failed);
      console.debug(
        "Pushed promise in failed queue...",
        JSON.stringify(failed)
      );
    },
    reinit(state: AuthState): void {
      state.failedQueue = [];
    },
    networkError(state: AuthState, isErr: boolean): void {
      state.networkError = isErr;
      isErr && console.error("Network error!");
    },
  },
  actions: {
    async firstLogIn({ dispatch, commit }, password: string): Promise<void> {
      try {
        await http.post("/auth/password", { password: password });
        await dispatch("logIn", password);
      } catch (e) {
        commit("logOut");
        throw e;
      }
    },
    async logIn({ commit }, password: string) {
      try {
        const res = await http.post("/auth/login", { password: password });
        if (res && res.status == 200) commit("logIn");
        else throw Error("Login failed");
      } catch (err) {
        console.error(err);
        throw err;
      }
    },
    async logOut({ commit }) {
      commit("logOut");
      await http.post("/auth/logout"); // to reset the cookie
    },
    async changePassword(
      { dispatch, commit },
      p: { newPassword: string; oldPassword?: string }
    ) {
      //console.debug("changing password from", p.oldPassword, "to", p.newPassword);
      const res = await http.put("/auth/password", {
        oldPassword: p.oldPassword,
        newPassword: p.newPassword,
      });
      if (res && res.status == 200) {
        commit("changingPassword", false);
        await dispatch("logIn", p.newPassword);
      }
    },
    async isFirstLogin({ commit }) {
      commit("firstLogIn", false);
      commit("networkError", false);
      const response = await http.get("/auth/password");
      commit("firstLogIn", response.data.mustChange ? true : false);
    },
    setup401Interceptor({ dispatch, commit }) {
      commit("reinit"); // empty the failed queue
      http.interceptors.response.use(
        (response) => response,
        (error) => {
          if (
            error == undefined ||
            !error.response ||
            error.response.status !== 401 ||
            error.config._retry
          ) {
            if (error?.message == "Network Error") {
              // is the above test working in all browsers?
              commit("networkError", true);
            }
            return Promise.reject(error);
          }
          const originalRequest = error.config;
          originalRequest._retry = true;
          console.debug("intercepted 401 on", JSON.stringify(originalRequest));
          console.error("Not logged!");
          return new Promise((resolve, reject) => {
            // push the failed request to failed queue
            commit("addFailed", { resolve, reject });
            dispatch("logOut"); // open the login popup
          })
            .then(() => {
              // relaunch the request and return it as the real result of the call that failed with a 401
              // This is triggered by the logIn action
              console.debug("restarting", JSON.stringify(originalRequest));
              return http(originalRequest);
            })
            .catch((error) => {
              console.error(error);
              return Promise.reject(error);
            });
        }
      );
    },
  },
};
const auth: ModuleTree<AuthState> = {
  auth: authModule,
};
export default auth;
