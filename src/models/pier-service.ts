// eslint-disable-next-line @typescript-eslint/no-explicit-any
type PierServiceBase = Record<string, any>;
export interface PierService extends PierServiceBase {
  activated: boolean;
  https?: boolean;
  challenge: "tls" | "http";
}

export type PierServices = Record<string, PierService>;
