import { http, ResponsePromise } from "@/utils/http-commons";

export class AboutService {
  static async about(): ResponsePromise {
    return http.get("/about");
  }
}
