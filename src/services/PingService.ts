import { http } from "@/utils/http-commons";
import { Vue } from "vue-class-component";

type Reloader = ReturnType<typeof setTimeout>;

function getSeconds(time: string | number): number {
  if (typeof time == "number") return time;
  const parsed = time.match(/(\d+)\s*([dhms])/);
  if (parsed) {
    const val = parseInt(parsed[1]);
    const unit = parsed[2];
    const mult =
      unit === "d" ? 86400 : unit === "h" ? 3600 : unit === "m" ? 60 : 1;
    return val * mult;
  } else throw "Malformed time string";
}

class PingService {
  // default = 3s
  async ping(timeout: string | number = 3) {
    return http.get("/admin/ping", { timeout: getSeconds(timeout) * 1000 });
  }
  // default = 5m
  async keepAlive(interval: string | number = 300) {
    setInterval(() => {
      this.ping().catch((err) => {
        console.log("keep alive error:", err);
      });
    }, getSeconds(interval) * 1000);
  }
  async reload(vue: Vue, remaining: number, step: number) {
    while (remaining > 0) {
      remaining -= step;
      try {
        await this.ping(step);
        // If here, we can contact the server, so force reload the vue and return...
        console.debug("Reloading page");
        window.location.reload();
        vue.$router.go(0);
        break;
      } catch (err: any) {
        // timeout ==> retry
        if (err.code == "ETIMEDOUT") continue;
        console.log("connexion lost");
        throw err;
      }
    }
  }
  reloadVueWhenPossible(
    vue: Vue,
    after: string | number,
    timeout: string | number = 120,
    step: string | number = 5
  ): Reloader {
    return setTimeout(() => {
      this.reload(vue, getSeconds(timeout), getSeconds(step));
    }, getSeconds(after) * 1000);
  }
  forget(when: Reloader) {
    clearTimeout(when);
  }
  async getCmdResults(cmdId: number) {
    return http.get(`/admin/cmdresult/${cmdId}`);
  }
}

export default new PingService();
