import {
  http,
  Body,
  shttp,
  HttpStreamer,
  ResponsePromise,
} from "@/utils/http-commons";
export async function getDomainAndContact(): ResponsePromise {
  return http.get("/admin/network/domain-and-contact");
}
export async function updateDomainAndContact(data: Body): ResponsePromise {
  return http.put("/admin/network/domain-and-contact", data);
}
export async function getDefaultNetworkInterface(): ResponsePromise {
  return http.get("/admin/network/interfaces/default");
}
export async function changeNetworkInterface(data: Body): ResponsePromise {
  return http.put("/admin/network/interfaces/default", data);
}
export async function ethernetConnected(): ResponsePromise {
  return http.get("/admin/network/ethernet/connected");
}
export async function scanWifi(): ResponsePromise {
  return http.get("/admin/network/wifi");
}
export async function addWifiNetwork(
  data: Body,
  streamer: HttpStreamer
): Promise<void> {
  return shttp.post("/admin/network/wifi", data, streamer);
}
