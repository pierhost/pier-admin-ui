import { Body, http } from "@/utils/http-commons";
class WireguardService {
  async createPeer() {
    return http.post("wireguard/peers");
  }
  async getPeers() {
    return http.get("wireguard/peers");
  }
  async removePeer(id: number) {
    return http.delete("wireguard/peers/" + id);
  }
  async updatePeer(id: number, data: Body) {
    return http.put("wireguard/peers/" + id, data);
  }
  async addExistingPeer(data: Body) {
    return http.put("wireguard/peers", data);
  }
}

export default new WireguardService();
