import { http, ResponsePromise } from "@/utils/http-commons";

export async function getContainers(): Promise<ResponsePromise> {
  return http.get("/containers");
}
export async function restartContainer(id: string): Promise<ResponsePromise> {
  return http.post(`/containers/${id}/restart`);
}
export async function getContainerLogs(id: string): Promise<ResponsePromise> {
  return http.get(`/containers/${id}/logs`);
}
