import {
  http,
  Body,
  shttp,
  HttpStreamer,
  ResponsePromise,
} from "@/utils/http-commons";

function streamUpdateThing(thing: string, streamer: HttpStreamer) {
  return shttp.post("/admin/update/" + thing, "", streamer);
}
export function streamUpdateAdmin(streamer: HttpStreamer): Promise<void> {
  return streamUpdateThing("admin", streamer);
}
export function streamUpdateUser(streamer: HttpStreamer): Promise<void> {
  return streamUpdateThing("user", streamer);
}
export function streamUpdateCritical(streamer: HttpStreamer): Promise<void> {
  return streamUpdateThing("critical", streamer);
}
export function streamUpdateAll(streamer: HttpStreamer): Promise<void> {
  return streamUpdateThing("all", streamer);
}
export async function getAdminConfig(): ResponsePromise {
  return http.get("/admin/config/admin");
}
export async function getUserConfig(): ResponsePromise {
  return http.get("/admin/config/user");
}
export async function getCriticalConfig(): ResponsePromise {
  return http.get("/admin/config/critical");
}
export async function updateCriticalConfig(data: Body): ResponsePromise {
  return http.post("/admin/config/critical", data);
}
export async function updateAdminConfig(data: Body): ResponsePromise {
  return http.post("/admin/config/admin", data);
}
export async function updateUserConfig(data: Body): ResponsePromise {
  return http.post("/admin/config/user", data);
}
export async function getNewServices(): ResponsePromise {
  return http.get("/admin/config/user?check-new=1");
}
export async function streamUpdateSystem(
  streamer: HttpStreamer
): Promise<void> {
  return shttp.post("/admin/system/update", "", streamer);
}
export async function streamRebootServer(
  streamer: HttpStreamer
): Promise<void> {
  return shttp.post("/admin/system/reboot", "", streamer);
}
export async function getAllPasswords(): ResponsePromise {
  return http.get("/admin/passwords");
}
export async function getWsdLogs(maxlines = 100): ResponsePromise {
  return http.get("admin/wsdlogs?maxlines=" + maxlines);
}
