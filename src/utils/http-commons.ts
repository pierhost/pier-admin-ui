import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { IncomingMessage } from "http";
import getEnv from "./env";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const streamHttp = require("stream-http");
// import streamHttp from "stream-http";
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Body = any;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type ResponsePromise = Promise<AxiosResponse<any>>;

const apiUrl = getEnv("VUE_APP_API_URL") || "API URL NOT DEFINED!";
console.info("API_URL=", apiUrl);
const axiosConfig: AxiosRequestConfig = {
  baseURL: apiUrl,
  withCredentials: true,
  headers: {
    "Content-Type": "application/json;charset=utf-8",
    "Access-Control-Allow-Origin": "*",
  },
};
export const http = axios.create(axiosConfig);
// fix wrong axios error code in case of timeout
http.interceptors.response.use(
  (response) => response,
  (error) => {
    if (
      error &&
      error.code == "ECONNABORTED" &&
      (error.message as string).startsWith("timeout of")
    )
      error.code = "ETIMEDOUT";
    return Promise.reject(error);
  }
);

type ResolveType = () => void;
type RejectType = (reason?: unknown) => void;
export interface HttpStreamer {
  response?: IncomingMessage;
  onData(buffer: Buffer): void;
  onEnd(resolve: ResolveType, reject: RejectType): void;
}
class StreamHttp {
  get(url: string, streamer: HttpStreamer): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      streamHttp.get(url, (res: IncomingMessage) => {
        res.on("data", streamer.onData);
        res.on("end", () => streamer.onEnd(resolve, reject));
      });
    });
  }
  post(path: string, data: string, streamer: HttpStreamer): Promise<void> {
    const base = apiUrl.startsWith("http") ? "" : location.origin;
    const url = new URL(base + apiUrl + path);
    console.log("stream url =", url);
    const options = {
      hostname: url.hostname,
      port: url.port,
      path: url.pathname,
      protocol: url.protocol,
      method: "POST",
      withCredentials: true,
      headers: {
        "Content-Type": "application/json;charset=utf-8",
        "Access-Control-Allow-Origin": "*",
        "Content-Length": Buffer.byteLength(data),
        TE: "trailers",
      },
    };
    return new Promise<void>((resolve, reject) => {
      const req = streamHttp.request(options, (res: IncomingMessage) => {
        console.log(`STATUS: ${res.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
        res.setEncoding("utf8");
        console.debug("res:", res, "streamer:", streamer);
        streamer.response = res;
        res.on("data", (data: Buffer) => {
          //console.debug("received", data);
          streamer.onData(data);
        });
        res.on("end", () => {
          streamer.onEnd(resolve, reject);
        });
      });
      req.on("error", (e: Error) => {
        console.error(`problem with request: ${e.message}`);
      });
      // Write data to request body
      req.write(data);
      req.end();
    });
  }
}

export const shttp = new StreamHttp();
