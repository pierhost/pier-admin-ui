declare global {
  interface Window {
    configs: Record<string, string>;
  }
}
export default function getEnv(name: string): string | undefined {
  const val = window?.configs?.[name] || process.env[name];
  console.debug(name, "=", val);
  return val;
}
