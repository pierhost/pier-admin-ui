import { RouteRecordRaw } from "vue-router";
import Tools from "@/views/Tools.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/tools",
    name: "Tools",
    component: Tools,
  },
];

export default routes;
