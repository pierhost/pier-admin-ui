import { RouteRecordRaw } from "vue-router";
import Config from "../views/Config.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/config",
    name: "Config",
    component: Config,
  },
];

export default routes;
