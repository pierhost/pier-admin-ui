import { RouteRecordRaw } from "vue-router";
import Network from "@/views/Network.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/network",
    name: "Network",
    component: Network,
  },
];

export default routes;
