import { RouteRecordRaw } from "vue-router";
import Admin from "@/views/Admin.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/admin",
    name: "Admin",
    component: Admin,
  },
];

export default routes;
