import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import PierHome from "../views/PierHome.vue";
import wgRoutes from "./wg-routes";
import pierRoutes from "./pier-routes";
import toolsRoutes from "./tools-routes";
import adminRoutes from "./admin-routes";
import networkRoutes from "./network-routes";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "PierHome",
    component: PierHome,
  },
  ...wgRoutes,
  ...pierRoutes,
  ...toolsRoutes,
  ...adminRoutes,
  ...networkRoutes,
];

console.info("Base URL=", process.env.BASE_URL);

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
