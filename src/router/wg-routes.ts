import { RouteRecordRaw } from "vue-router";
import Wireguard from "@/views/Wireguard.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/wg",
    name: "Wireguard",
    component: Wireguard,
  },
];

export default routes;
