#!/bin/sh

# replace the API URL based on envt value in index.html

JSON_STRING='window.configs = { \
  "VUE_APP_API_URL":"'${VUE_APP_API_URL}'" \
}'
sed -i "s@// CONFIGURATIONS_PLACEHOLDER@${JSON_STRING}@" /usr/share/nginx/html/index.html
